const shim = require('fabric-shim');

class MyChaincode {
  async Init(stub) {
    let ret = stub.getFunctionAndParameters();
    let args = ret.params;
    if (args.length != 2) {
      return shim.error("Error. Invalid call arguments.")
    }

    // save the initial states
    await stub.putState(args[0], Buffer.from(args[1]));
    return shim.success(Buffer.from('Initialized Successfully!'));
  }

  async Invoke(stub) {
    let functionAndParams = stub.getFunctionAndParameters();
    if (functionAndParams.fcn == "set"){
      return this.set(stub, functionAndParams.params)
    } else if (functionAndParams.fcn == "get"){
      return this.get(stub, functionAndParams.params)
    }else{
      return shim.error("Invalid Invocation")
    }
  }

  async get(stub, params) {
    let res = await stub.getState(params[0]);
    return shim.success(res);


  }
  async set(stub, params) {
    if(params.length != 2) {
      return shim.error("Error. Invalid number of arguments.");
    }
    await stub.putState(params[0], params[1]);

    return shim.success(Buffer.from('Set Successfully!'));
  }
};

module.exports = MyChaincode;
//shim.start(new MyChaincode());