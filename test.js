var MyChaincode = require('./mycc')
var ccmockstub = require("@theledger/fabric-mock-stub")
var chai = require("chai")
var expect = chai.expect;

// You always need your chaincode so it knows which chaincode to invoke on
const chaincode = new MyChaincode();

describe('Test MyChaincode', () => {

    const mockStub = new ccmockstub.ChaincodeMockStub("MyMockStub", chaincode);

    it("Should init without issues", async () => {
        const responseInit = await mockStub.mockInit("Init", ["abc","abc","50"]);
        expect(ccmockstub.Transform.bufferToString(responseInit.payload)).to.eql("Initialized Successfully!")
    });

    it("Should Set without issues", async () => {
        const responseSet = await mockStub.mockInvoke("tx2", ["set","abc",100]);
        expect(ccmockstub.Transform.bufferToString(responseSet.payload)).to.eql("Set Successfully!")
    });

    it("Should Get without issues", async () => {
        const responseGet = await mockStub.mockInvoke("tx3", ["get","abc"]);
        expect(parseInt(ccmockstub.Transform.bufferToString(responseGet.payload))).to.eql(100)
    });
});